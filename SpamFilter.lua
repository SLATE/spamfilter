﻿-------------
-- Globals --
-------------
if (SpamFilter == nil) then SpamFilter = {} end
SpamFilter.savedVarsName = "SpamFilter_SavedVariables"
SpamFilter.internalDefault = function()
	return {
		debug = false,
		active = true,
		enableDefaultFilters = true
	}
end

SpamFilter.defaultFilters =
	{
		["SpamFilterDefault1"] = "%s*[vw]+%.%w-g[o0][l1]d%w*%.c[o0]m%s*",
		["SpamFilterDefault2"] = "mm[o0]w[i1]n%.com",
		["SpamFilterDefault3"] = "g[o0][l1]dah%.c[o0]m",
		["SpamFilterDefault4"] = "g[o0][l1]dce[o0]%.c[o0]m",
		["SpamFilterDefault5"] = "tesg[o0][l1]dma[l1][l1]",
		["SpamFilterDefault6"] = "currency[o0]ffer",
		["SpamFilterDefault7"] = "www%.pvpb[aá]nk%.c[óo0]m",
		["SpamFilterDefault8"] = "www%.gameim%.com",
		["SpamFilterDefault9"] = "www%.g4ez%.com",
		["SpamFilterDefault10"] = "g[o0]ldaa%.c[o0]m",

	}

SpamFilter.filtersCustom =
{
	data = {}
}

SpamFilter.filteredChannels = function()
	return {
		CHAT_CHANNEL_ZONE,
		CHAT_CHANNEL_SAY,
		CHAT_CHANNEL_YELL,
	}
end
SpamFilter.savedVars = {}

SpamFilter.noteNew = nil
SpamFilter.ignoreQueue = {}
SpamFilter.ignoreQueueRunning = false

-------------
-- Utility --
-------------

function SpamFilter.InitializeSavedVars()
	SpamFilter.savedVars =
	{
		["internal"] = ZO_SavedVars:NewAccountWide(SpamFilter.savedVarsName, 1, "internal", SpamFilter.internalDefault()),
		["filters"] = ZO_SavedVars:NewAccountWide(SpamFilter.savedVarsName, 1, "filters", SpamFilter.filtersCustom),
	}
	SpamFilter.Debug(SpamFilter.savedVars)
end

local function IsIn(value, array)
	SpamFilter.Debug(value)
	SpamFilter.Debug(array)
	if (#array == 0) then
		return false
	end
	for i = 1, #array do
		if (array[i] == value) then
			return true
		end
	end
	return false
end

local function Trim(s)
	return (s:gsub("^%s*(.-)%s*$", "%1"))
end

local function RuleBroken(text)
	for key, value in pairs(SpamFilter.savedVars["filters"].data) do
		if (string.find(string.lower(text), value) ~= nil) then
			return key
		end
	end
	if (SpamFilter.savedVars["internal"].enableDefaultFilters) then
		for key, value in pairs(SpamFilter.defaultFilters) do
			if (string.find(string.lower(text), value) ~= nil) then
				return key
			end
		end
	end
	return nil
end

local function Ternary(cond, ifTrue, ifFalse)
	if (cond) then return ifTrue end
	return ifFalse
end

local function EmitMessage(message)
	if (CHAT_SYSTEM) then
		if (message == "") then
			message = "[Empty String]"
		end
		CHAT_SYSTEM:AddMessage(message)
	end
end

local function EmitTable(t, indent, tableHistory)
	indent = indent or "."
	tableHistory = tableHistory or {}
	for k, v in pairs(t) do
		local vType = type(v)
		EmitMessage(indent.."("..vType.."): "..tostring(k).." = "..tostring(v))
		if (vType == "table") then
			if (tableHistory[v]) then
				EmitMessage(indent.."Avoiding circular reference...")
			else
				tableHistory[v] = true
				EmitTable(v, indent.." ", tableHistory)
			end
		end
	end
end

local function GetCustomFiltersString()
	local ret = ""
	for name, pattern in pairs(SpamFilter.savedVars["filters"].data) do
		ret = ret..string.format("\n%s: %s", name, pattern)
	end
	if ret == "" then
		return "example: www.somebaddomain.com"
	end
	return ret:sub(2)
end

local function ResetCustomFilter()
	GetWindowManager():GetControlByName("SpamFilter_CustomFiltersEdit"):SetText(GetCustomFiltersString())
end

local function ClearCustomFilter()
	SpamFilter.savedVars["filters"].data = {}
	ResetCustomFilter()
end

local function ParseCustomFilter()
	local patterns = GetWindowManager():GetControlByName("SpamFilter_CustomFiltersEdit"):GetText()
	SpamFilter.savedVars["filters"].data = {}
	for name, pattern in patterns:gmatch("([^\n:]+)%s*:%s*([^\n]+)%s*") do
		if name ~= "example" then
			SpamFilter.savedVars["filters"].data[name] = Trim(pattern)
		end
	end
	ResetCustomFilter()
end

function SpamFilter.Debug(...)
	if (SpamFilter.savedVars["internal"].debug) then
		SpamFilter.Emit(...)
	end
end

function SpamFilter.Emit(...)
	for i = 1, select("#", ...) do
		local value = select(i, ...)
		if (type(value) == "table") then
			EmitTable(value)
		else
			EmitMessage(tostring(value))
		end
	end
end

local function LoadStrings()
	local lang = GetCVar("language.2") or "en"
	local supported = false
	for k, _ in pairs(SpamFilter.Lang) do
		if k == lang then supported = true end
	end
	if supported == false then
		SpamFilter.Debug("Language '"..lang.."' is not supported. Reverting to 'en'.")
		lang = "en"
	end
	
	SpamFilter.SlashCommands = SpamFilter.Lang[lang].SlashCommands
	SpamFilter.Arguments = SpamFilter.Lang[lang].Arguments
	SpamFilter.EmitStrings = SpamFilter.Lang[lang].EmitStrings
	SpamFilter.OptionsStrings = SpamFilter.Lang[lang].OptionsStrings
end

function SpamFilter.GetEnabled()
	return SpamFilter.savedVars["internal"].active
end

function SpamFilter.SetEnabled(checkbox)
	SpamFilter.savedVars["internal"].active = checkbox
	SpamFilter.Emit(string.format(SpamFilter.EmitStrings.spamFilterStatus, Ternary(SpamFilter.savedVars["internal"].active, SpamFilter.Arguments.active, SpamFilter.Arguments.inactive)))
end

function SpamFilter.GetDebugEnabled()
	return SpamFilter.savedVars["internal"].debug
end

function SpamFilter.SetDebugEnabled(checkbox)
	SpamFilter.savedVars["internal"].debug = checkbox
	SpamFilter.Emit(string.format(SpamFilter.EmitStrings.debugStatus, Ternary(SpamFilter.savedVars["internal"].debug, SpamFilter.Arguments.on, SpamFilter.Arguments.off)))
end

function SpamFilter.ShowIgnoreList()
	local numIgnored = GetNumIgnored()
	for i = 1, numIgnored do
		local name, note = GetIgnoredInfo(i)
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.ignoredFormat, name, note))
	end
end

function SpamFilter.GetEnableDefaultFilters()
	return SpamFilter.savedVars["internal"].enableDefaultFilters
end

function SpamFilter.SetEnableDefaultFilters(checkbox)
	SpamFilter.savedVars["internal"].enableDefaultFilters = checkbox
	SpamFilter.Emit(string.format(SpamFilter.EmitStrings.defaultFiltersStatus, Ternary(SpamFilter.savedVars["internal"].enableDefaultFilters, SpamFilter.Arguments.on, SpamFilter.Arguments.off)))
end

function SpamFilter.ShowCustomFilters()
	SpamFilter.Emit(SpamFilter.EmitStrings.customFilters)
	for key, value in pairs(SpamFilter.savedVars["filters"].data) do
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.filterFormat, key, value))
	end
end

function SpamFilter.ShowDefaultFilters()
	SpamFilter.Emit(SpamFilter.EmitStrings.defaultFilters)
	for key, value in pairs(SpamFilter.defaultFilters) do
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.filterFormat, key, value))
	end
end

--------------------
-- Slash Commands --
--------------------
local function AddSlashCommands()
	SLASH_COMMANDS[SpamFilter.SlashCommands.sfdebug] = function(extra)
		if (extra == nil or extra == "") then
			SpamFilter.savedVars["internal"].debug = not SpamFilter.savedVars["internal"].debug
		elseif (extra == SpamFilter.Arguments.on) then
			SpamFilter.savedVars["internal"].debug = true
		elseif (extra == SpamFilter.Arguments.off) then
			SpamFilter.savedVars["internal"].debug = false
		else
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.invalidArg, tostring(extra)))
			return
		end
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.debugStatus,
			Ternary(SpamFilter.savedVars["internal"].debug, SpamFilter.Arguments.on, SpamFilter.Arguments.off)))
	end

	SLASH_COMMANDS[SpamFilter.SlashCommands.sffilters] = function(extra)
		local strExtra = tostring(extra)
		SpamFilter.Emit(SpamFilter.EmitStrings.currentFilters)
		if (strExtra == nil or strExtra == "") then
			SpamFilter.ShowDefaultFilters()
			SpamFilter.ShowCustomFilters()
		elseif (strExtra == SpamFilter.Arguments.default) then
			SpamFilter.ShowDefaultFilters()
		elseif (strExtra == SpamFilter.Arguments.custom) then
			SpamFilter.ShowCustomFilters()
		else
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.invalidArg, strExtra))
		end
	end

	SLASH_COMMANDS[SpamFilter.SlashCommands.sfignorelist] = function()
		local numIgnored = GetNumIgnored()
		for i = 1, numIgnored do
			local name, note = GetIgnoredInfo(i)
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.ignoredFormat, name, note))
		end
	end

	SLASH_COMMANDS[SpamFilter.SlashCommands.sfadd] = function(extra)
		local strExtra = tostring(extra)
		if (strExtra == nil or strExtra == "") then
			SpamFilter.Emit(SpamFilter.EmitStrings.sfaddFormat)
			return
		end
		local name, def = string.match(strExtra, "(%S+)(.*)")
		SpamFilter.savedVars["filters"].data[name] = Trim(def)
		SpamFilter.Debug(SpamFilter.savedVars["filters"].data)
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.filterAdded, name))
		ResetCustomFilter()
	end

	SLASH_COMMANDS[SpamFilter.SlashCommands.sfremove] = function(extra)
		local name = tostring(extra)
		if (name == nil or name == "") then
			SpamFilter.Emit(SpamFilter.EmitStrings.sfremoveFormat)
			return
		end
		SpamFilter.savedVars["filters"].data[name] = nil
		SpamFilter.Debug(SpamFilter.savedVars["filters"].data)
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.filterRemoved, name))
		ResetCustomFilter()
	end

	SLASH_COMMANDS[SpamFilter.SlashCommands.sfclear] = function()
		SpamFilter.savedVars["filters"].data = {}
		SpamFilter.Debug(SpamFilter.savedVars["filters"].data)
		SpamFilter.Emit(SpamFilter.EmitStrings.filtersCleared)
		ResetCustomFilter()
	end

	SLASH_COMMANDS[SpamFilter.SlashCommands.sfactive] = function(extra)
		if (extra == nil or extra == "") then
			SpamFilter.savedVars["internal"].active = not SpamFilter.savedVars["internal"].active
		elseif (extra == SpamFilter.Arguments.on) then
			SpamFilter.savedVars["internal"].active = true
		elseif (extra == SpamFilter.Arguments.off) then
			SpamFilter.savedVars["internal"].active = false
		else
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.invalidArg, tostring(extra)))
			return
		end
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.spamFilterStatus, Ternary(SpamFilter.savedVars["internal"].active, SpamFilter.Arguments.active, SpamFilter.Arguments.inactive)))
	end
	
	SLASH_COMMANDS[SpamFilter.SlashCommands.sfdefault] = function(extra)
		if (extra == nil or extra == "") then
			SpamFilter.savedVars["internal"].enableDefaultFilters = not SpamFilter.savedVars["internal"].enableDefaultFilters
		elseif (extra == SpamFilter.Arguments.on or extra == "on") then
			SpamFilter.savedVars["internal"].enableDefaultFilters = true
		elseif (extra == SpamFilter.Arguments.off or extra == "off") then
			SpamFilter.savedVars["internal"].enableDefaultFilters = false
		else
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.invalidArg, tostring(extra)))
			return
		end
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.defaultFiltersStatus, Ternary(SpamFilter.savedVars["internal"].enableDefaultFilters, SpamFilter.Arguments.on, SpamFilter.Arguments.off)))
	end
end

local function ProcessIgnoreQueue()
	for name, _ in pairs(SpamFilter.ignoreQueue) do
		AddIgnore(name)
		SpamFilter.ignoreQueue[name] = nil
		zo_callLater(ProcessIgnoreQueue, 2000)
		return
	end
	SpamFilter.ignoreQueueRunning = false
end

-------------------
-- Event Methods --
-------------------
local function OnMessageReceived(eventCode, messageType, fromName, text)
	if (SpamFilter.savedVars["internal"].active and IsIn(messageType, SpamFilter.filteredChannels()) and not IsFriend(fromName)) then
		SpamFilter.Debug("Validating message from "..fromName..".")
		local ruleBroken = RuleBroken(text)
		if (not IsIgnored(fromName) and ruleBroken ~= nil) then
			-- Queue the player to be ignored ...
			SpamFilter.ignoreQueue[fromName] = true
			
			if not SpamFilter.ignoreQueueRunning then
				zo_callLater(ProcessIgnoreQueue, 1);
				SpamFilter.ignoreQueueRunning = true
			end
			
			-- ... and queue the note to be set (since this is updated after event processing)
			SpamFilter.noteNew = string.format(SpamFilter.EmitStrings.note, ruleBroken, GetDateStringFromTimestamp(GetTimeStamp()), GetTimeString())
			
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.filtered, fromName, ruleBroken))
		end
	end
end

------------
-- Events --
------------

local function OnIgnoreAdded(_, userid)
	-- let's see if there are any notes to be set
	if SpamFilter.noteNew ~= nil then
		-- look for the correct ignore list entry
		for i = GetNumIgnored(), 1, -1 do
			local euserid = GetIgnoredInfo(i)
			if euserid == userid then
				-- cache the note and set it 2 seconds later (to prevent throttling kicking in)
				local note = SpamFilter.noteNew
				zo_callLater(function() SetIgnoreNote(i, note) end, 2000)
				SpamFilter.noteNew = nil
				return
			end
		end
	end
end

local function OnAddOnLoaded(eventCode, addOnName)
	if (addOnName ~= "SpamFilter") then
		return
	end
	SpamFilter.InitializeSavedVars()
	LoadStrings()
	AddSlashCommands()
	EVENT_MANAGER:RegisterForEvent("SpamFilter", EVENT_CHAT_MESSAGE_CHANNEL, OnMessageReceived)
	SpamFilter.Debug("SpamFilter initialized")

	-- options panel
	LAM = LibStub:GetLibrary("LibAddonMenu-1.0")
	SFpanel = LAM:CreateControlPanel("SpamFilter_OptionsPanel", SpamFilter.OptionsStrings.spamFilterOptions)
		LAM:AddHeader(SFpanel, "SpamFilter_AboutHeader", SpamFilter.OptionsStrings.about)
			LAM:AddDescription(SFpanel, "SpamFilter_AboutDesc", SpamFilter.OptionsStrings.aboutDesc)
		--LAM:AddHeader(SFpanel, "SpamFilter_OptionsHeader", SpamFilter.OptionsStrings.options)
			LAM:AddCheckbox(SFpanel, "SpamFilter_Option_Enable_SpamFilter", SpamFilter.OptionsStrings.enableFiltering, SpamFilter.OptionsStrings.enableFilteringTooltip, SpamFilter.GetEnabled, SpamFilter.SetEnabled)
		LAM:AddHeader(SFpanel, "SpamFilter_FilterHeader", SpamFilter.OptionsStrings.filter)
			LAM:AddDescription(SFpanel, "SpamFilter_FilterDesc", SpamFilter.OptionsStrings.filterDesc)
			LAM:AddCheckbox(SFpanel, "SpamFilter_Option_Enable_DefaultFilters", SpamFilter.OptionsStrings.enableDefaultFilters, SpamFilter.OptionsStrings.enableDefaultFiltersTooltip, SpamFilter.GetEnableDefaultFilters, SpamFilter.SetEnableDefaultFilters)
			LAM:AddDescription(SFpanel, "SpamFilter_CustomFilterDesc", SpamFilter.OptionsStrings.customFilterDesc)
			LAM:AddDescription(SFpanel, "SpamFilter_CustomFilterLabel", nil, SpamFilter.OptionsStrings.customFilters)
			LAM:AddEditBox(SFpanel, "SpamFilter_CustomFilters", nil, nil, true, function() end, function() end, false, nil)
			editbox = GetWindowManager():GetControlByName("SpamFilter_CustomFilters")
			editbox.bg:SetWidth(510)
			editbox.bg:SetAnchor(LEFT)
			editbox.edit:SetMaxInputChars(4096)
			editbox.edit:SetText(GetCustomFiltersString());
			LAM:AddButton(SFpanel, "SpamFilter_ApplyFilters", SpamFilter.OptionsStrings.apply, nil, ParseCustomFilter)
			LAM:AddButton(SFpanel, "SpamFilter_ResetFilters", SpamFilter.OptionsStrings.reset, nil, ResetCustomFilter)
			LAM:AddButton(SFpanel, "SpamFilter_ClearFilters", SpamFilter.OptionsStrings.clear, nil, ClearCustomFilter)
			
		LAM:AddHeader(SFpanel, "SpamFilter_DebugHeader", SpamFilter.OptionsStrings.debugging)
			LAM:AddDescription(SFpanel, "SpamFilter_DebugDesc", SpamFilter.OptionsStrings.debuggingDesc)
			LAM:AddCheckbox(SFpanel, "SpamFilter_Option_Enable_Debugging", SpamFilter.OptionsStrings.enableDebugging, SpamFilter.OptionsStrings.enableDebuggingTooltip, SpamFilter.GetDebugEnabled, SpamFilter.SetDebugEnabled)
			--LAM:AddButton(SFpanel, "SpamFilter_Option_ShowIgnoreList", SpamFilter.OptionsStrings.showIgnoreList, SpamFilter.OptionsStrings.showIgnoreListTooltip, SpamFilter.ShowIgnoreList)
			--LAM:AddButton(SFpanel, "SpamFilter_Option_ShowCustomFilters", SpamFilter.OptionsStrings.showCustomFilters, SpamFilter.OptionsStrings.showCustomFiltersTooltip, SpamFilter.ShowCustomFilters)
			--LAM:AddButton(SFpanel, "SpamFilter_Option_ShowDefaultFilters", SpamFilter.OptionsStrings.showDefaultFilters, SpamFilter.OptionsStrings.showDefaultFiltersTooltip, SpamFilter.ShowDefaultFilters)

	-- unregister event handler
	EVENT_MANAGER:UnregisterForEvent("SpamFilter", EVENT_ADD_ON_LOADED)
	
	-- register handler to write notes
	EVENT_MANAGER:RegisterForEvent("SpamFilter", EVENT_IGNORE_ADDED, OnIgnoreAdded)
end

EVENT_MANAGER:RegisterForEvent("SpamFilter", EVENT_ADD_ON_LOADED, OnAddOnLoaded)