﻿if (SpamFilter == nil) then SpamFilter = {} end

SpamFilter.Lang =
{
	["en"] = {},
	["de"] = {},
}

---------------------
-- English Strings --
---------------------

SpamFilter.Lang["en"].SlashCommands =
{
	sfdebug = "/sfdebug",
	sffilters = "/sffilters",
	sfignorelist = "/sfignorelist",
	sfadd = "/sfadd",
	sfremove = "/sfremove",
	sfclear = "/sfclear",
	sfactive = "/sfactive",
	sfdefault = "/sfdefault",
}

SpamFilter.Lang["en"].Arguments =
{
	on       = "on",
	off      = "off",
	active   = "active",
	inactive = "inactive",
	default  = "default",
	custom   = "custom",
}

SpamFilter.Lang["en"].EmitStrings =
{
	invalidArg           = "Invalid argument: %s",
	debugStatus          = "SpamFilter debugging is %s",
	defaultFiltersStatus = "SpamFilter default filters are %s",
	currentFilters       = "|cffffffSpamFilter - Current Filters",
	customFilters        = "|cffffffSpamFilter - Custom Filters",
	defaultFilters       = "|cffffffSpamFilter - Default Filters",
	filterFormat         = "  |cffffffName: |caaaaaa%s    |cffffffFilter: |caaaaaa%s",
	ignoredFormat        = "  |cffffffName: |caaaaaa%s    |cffffffNote: |caaaaaa%s",
	sfaddFormat          = "Command Format: /sfadd <name> <filter definition>",
	filterAdded          = "Filter added: %s",
	sfremoveFormat       = "Command Format: /sfremove <name>",
	filterRemoved        = "Filter removed: %s",
	defaultsRestored     = "Default filters restored.",
	filtersCleared       = "All filters removed",
	spamFilterStatus     = "SpamFilter is %s",
	filtered             = "|cff7777%s is now ignored due to violating rule '%s'.",
	note                 = "|caaaaaaBlocked by SpamFilter|r\n\nRule: |caaaaaa%s|r\nTimestamp: |caaaaaa%s %s",
}

SpamFilter.Lang["en"].OptionsStrings =
{
	spamFilterOptions           = "SpamFilter",
	about                       = "About",
	aboutDesc                   = "SpamFilter scans zone, say, and yell channels for gold spammers, and automatically adds them to your ignore list. Don't worry though - people in your friends list are automatically white-listed, so you don't have to worry about accidentally ignoring a friend.",
	options                     = "Options",
	enableFiltering             = "Enable Filtering",
	enableFilteringTooltip      = "When enabled, accounts that send messages that meet your filters are automatically ignored.",
	enableDefaultFilters        = "Enable default filter list",
	enableDefaultFiltersTooltip = "When enabled, the default filter list will be used in addition to any custom filters.\n\nThis option should stay enabled.",
	filter                      = "Filters",
	filterDesc                  = "Filters define which chat entries aren't acceptable.",
	customFilterDesc            = "You can define one filter per line. Always start with the filter's unique name, followed by a colon (:) and the actual filter pattern.",
	customFilters               = "Custom Filters",
	apply                       = "Apply",
	reset                       = "Reset",
	clear                       = "Clear",
	debugging                   = "Debugging",
	debuggingDesc               = "You should only touch the following options if you plan on working on the add-on. You're not missing out on anything by ignoring them!",
	showIgnoreList              = "Show Ignore List",
	showIgnoreListTooltip       = "Clicking this will display your current ignore list to the chat window.",
	showCustomFilters           = "Show Custom Filters",
	showCustomFiltersTooltip    = "Clicking this will display your current custom filters to the chat window.",
	showDefaultFilters          = "Show Default Filters",
	showDefaultFiltersTooltip   = "Clicking this will display the default filters to the chat window.",
	enableDebugging             = "Enable Debugging",
	enableDebuggingTooltip      = "Enable SpamFilter debug messages to chat window.\n\nThis option should stay disabled.",
}

--------------------
-- German Strings --
--------------------

SpamFilter.Lang["de"].Arguments =
{
	on       = "ein",
	off      = "aus",
	active   = "aktiv",
	inactive = "inaktiv",
	default  = "default",
	custom   = "eigen",
}

SpamFilter.Lang["de"].EmitStrings =
{
	invalidArg           = "Ungültiges Argument: %s",
	debugStatus          = "Der SpamFilter-Debugmodus ist %sgeschaltet.",
	defaultFiltersStatus = "Die SpamFilter-Standardfilter sind %sgeschaltet.",
	currentFilters       = "|cffffffSpamFilter - Aktuelle Filter",
	customFilters        = "|cffffffSpamFilter - Eigene Filter",
	defaultFilters       = "|cffffffSpamFilter - Standardfilter",
	filterFormat         = "  |cffffffName: |caaaaaa%s    |cffffffFilter: |caaaaaa%s",
	ignoredFormat        = "  |cffffffName: |caaaaaa%s    |cffffffNotiz: |caaaaaa%s",
	sfaddFormat          = "Syntax: /sfadd <name> <filterdefinition>",
	filterAdded          = "Der Filter '%s' wurde hinzugefügt.",
	sfremoveFormat       = "Syntax: /sfremove <name>",
	filterRemoved        = "Der Filter '%s' wurde entfernt",
	defaultsRestored     = "Die Standardfilter wurden wiederhergestellt.",
	filtersCleared       = "Alle Filter wurden entfernt.",
	spamFilterStatus     = "SpamFilter ist %s.",
	filtered             = "|cff7777%s hat gegen die Regel '%s' verstoßen und wird nun ignoriert.",
	note                 = "|caaaaaaDurch SpamFilter gesperrt|r\n\nRegel: |caaaaaa%s|r\nZeitpunkt: |caaaaaa%s %s",
}

SpamFilter.Lang["de"].OptionsStrings =
{
	spamFilterOptions           = "SpamFilter",
	about                       = "Über",
	aboutDesc                   = "SpamFilter sucht automatisch Goldverkäufer und Spammer in den Chatkanälen für Gebiet, Sagen und Rufen und fügt diese zu Eurer Liste ignorierter Spieler hinzu. Keine Sorge - Eure Freunde werden automatisch übergangen, Ihr müsst Euch also nicht sorgen, diese versehentlich zu ignorieren.",
	options                     = "Einstellungen",
	enableFiltering             = "SpamFilter aktivieren",
	enableFilteringTooltip      = "Im aktivierten Zustand wird SpamFilter Spieler automatisch ignorieren, wenn diese Euch Nachrichten zusenden, auf die mindetens eine Filterregel zutrifft.",
	enableDefaultFilters        = "Standardfilter verwenden",
	enableDefaultFiltersTooltip = "Legt fest, ob die Standardfilter zusätzlich zu den eigenen Filtern verwendet werden sollen.\n\nDiese Option sollte aktiviert bleiben.",
	filter                      = "Filter",
	filterDesc                  = "Filter definieren, welche Chateinträge nicht akzeptabel sind.",
	customFilterDesc            = "Ihr könnt einen Filter pro Zeile definieren. beginnt jeweils mit einem einzigartigen Namen für den Filter, gefolgt von einem Doppelpunkt (:) und dem jeweiligen Suchmuster.",
	customFilters               = "Eigene Filter",
	apply                       = "Übernehmen",
	reset                       = "Zurücksetzen",
	clear                       = "Löschen",
	debugging                   = "Debugmodus",
	debuggingDesc               = "Ihr solltet die folgenden Einstellungen einfach ignorieren, solange Ihr nicht an der Erweiterung arbeiten möchtet. Ihr verpasst dadurch nichts!",
	showIgnoreList              = "Ignorierte Spieler",
	showIgnoreListTooltip       = "Gibt alle aktuell ignorierten Spieler im Chatfenster aus.",
	showCustomFilters           = "Eigene Filter",
	showCustomFiltersTooltip    = "Gibt alle eigenen Filter im Chatfenster aus.",
	showDefaultFilters          = "Standardfilter",
	showDefaultFiltersTooltip   = "Gibt alle Standardfilter im Chatfenster aus.",
	enableDebugging             = "Debugmodus",
	enableDebuggingTooltip      = "Legt fest, ob SpamFilter interne Statustexte ins Chatfenster schreiben soll.\n\nDiese Option sollte deaktiviert bleiben.",
}

--------------------------
-- Fill missing strings --
--------------------------

for cat, ecat in pairs(SpamFilter.Lang["en"]) do
	for lc, loc in pairs(SpamFilter.Lang) do
		if lc ~= "en" then
			if loc[cat] == nil then
				loc[cat] = {}
			end
			for k, v in pairs(ecat) do
				if loc[cat][k] == nil then
					loc[cat][k] = v
				end
			end
		end
	end
end
